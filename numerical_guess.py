Описание проекта: программа генерирует случайное число в диапазоне от 11 до 100100 и просит пользователя угадать это число. 
Если догадка пользователя больше случайного числа, то программа должна вывести сообщение 'Слишком много, попробуйте еще раз'.
Если догадка меньше случайного числа, то программа должна вывести сообщение 'Слишком мало, попробуйте еще раз'.
Если пользователь угадывает число, то программа должна поздравить его и вывести сообщение 'Вы угадали, поздравляем!'.
...

import random

number = random.randint(0, 100)
print('Добро пожаловать в числовую угадайку :)')

def is_valid(num):
    if num.isdigit():
        num = int(num)
        if 1 <= num <= 100:
            return True
        else:
            return False
    else:
        return False

while True:
    user_num = input('Введите целое число от 1 до 100:')
    if not is_valid(user_num):
        print('Не пытайтесь меня обмануть, введите число от 1 до 100 :)')
        continue
    user_num = int(user_num)

    if user_num < number:
        print('Ваше число меньше загаданного, попробуйте еще разок')
    elif user_num > number:
        print('Ваше число больше загаданного, попробуйте еще разок')
    else:
        print('Вы угадали, поздравляем!')
        break

print('Спасибо, что играли в числовую угадайку. Еще увидимся...')
